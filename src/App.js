import React from 'react'
import MaquinaCafe from './components/MaquinaCafe'

export default class App extends React.Component{

  constructor (propos) {
    super(propos)

    this.state = {
      cafesFeitos:0
    }
  }

  adicionarCafe = ()=> {
      this.setState({
        cafesFeitos: this.state.cafesFeitos + 1
      })
  }
 
  render = () => {
    return <main>
        <h1>Faça aqui o seu café</h1>
          <MaquinaCafe numCafesFeitos = {this.state.cafesFeitos} adicionar = {this.adicionarCafe}></MaquinaCafe>
    </main>
    
  }
}
 